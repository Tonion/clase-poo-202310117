<?php

abstract Class Platillo{


abstract protected function Preparacion();

}

Class PapasConCarne Extends Platillo{

public function Preparacion(){
    echo "</br>RECETA PARA HACER PAPA ASADA CON CARNE:</br>";
    echo "</br>1. Lavar las papas, envolverlas en aluminio y llevarlas a un horno a 200°</br>";
    echo "</br>2. Sacar las papas y dejarlas enfriar</br>";
    echo "</br>3. Se pica o se tritura cebolla y se colocan en un sartén con aceite y un poco de sal.</br>";
    echo "</br>4. Se añade tomate triturado hasta que se reduzca un poco y se ponga espeso</br>";
    echo "</br>5. Se agrega la carne picada junto con el orégano, sal y pimienta al gusto</br>";
    echo "</br>6. Se retira la carne del fuego cuando esté bien dorada</br>";
    echo "</br>7. Se procede a retirar la pulpa o el relleno de las papas</br>";
    echo "</br>8. El sofrito hecho anteriormente, lo añadiremos a cada mitad de papa como relleno</br>";
}

}
Class HamburguesaCamaron Extends Platillo{

    public function Preparacion(){
        echo "</br>RECETA PARA HACER HAMBURGUESA DE CAMARÓN:</br>";
        echo "</br>1. Coloca el aguacate con el jugo de limón, sal y pimienta y presiona con una cuchara hasta obtener un puré:</br>";
        echo "</br>2. Mezcla los camarones limpios con pimienta y paprika uniformemente. Añade el ajo, la cebolla, el chile habanero, la miel, la hierbabuena y la Salsa Teriyaki</br>";
        echo "</br>3. Refrigerar 10 minutos</br>";
        echo "</br>4. Vierte el aceite de ajonjolí sobre una sartén parrilla caliente</br>";
        echo "</br>5. Coloca los camarones marinados junto con la cebolla en aros, cocina por 6 minutos</br>";
        echo "</br>6. Parrilla las rebanadas de piña por ambos lados en el mismo sartén caliente</br>";
        echo "</br>7. Coloca las bases del pan para hamburguesa sobre una charola engrasada, baña con mantequilla y coloca los camarones cocidos</br>";
        echo "</br>8. Cubre con queso rallado y gratina por 10 minutos en el horno, a 180 °C</br>";
        echo "</br>9. Arma tu hamburguesa y listo</br>";
    }
    
    }

$obj = new PapasConCarne();
$obj2 = new HamburguesaCamaron();
$obj->Preparacion();
$obj2->Preparacion();

?>