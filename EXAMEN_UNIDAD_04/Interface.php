<?php
//Interfaz que define los métodos a usar
interface Operaciones{
    public function Cuadrado($cantidad);
    public function Raiz($cantidad);
}
//Métodos con su función a realizar
  class MatesBasicas implements Operaciones{
    public function Cuadrado($cantidad){
        return $cantidad**2;
        }
    public function Raiz($cantidad){
        return sqrt($cantidad);
        }
}
//Mostrar los resultados
$obj = new MatesBasicas();
echo "<br> Tu cantidad elevada al cuadrado: ";
echo $obj->Cuadrado(36);
echo "<br>La raíz cuadrada de tu cantidad: ";
echo $obj->Raiz(36);
?>