<?php


abstract Class Transporte{


    abstract protected function Mantenimiento();

}

Class Avion Extends Transporte{

    public function Mantenimiento(){
        echo "</br>1.- Mantenimiento a un avion:</br>";
        echo "</br>Revisiones en tránsito</br>";
        echo "</br>Revisiones diarias</br>";
        echo "</br>Revisiones de 48 horas</br>";
    }

}

Class Helicoptero Extends Transporte{

    public function Mantenimiento(){
        echo "</br>1.- Mantenimiento a un helicoptero:</br>";
        echo "</br>Cambio de componentes</br>";
        echo "</br>Reparación de algún daño estructural</br>";
        echo "</br>Engrases, limpieza, comprobación de niveles de aceite, entre otros</br>";
    }

}

Class Bicicleta extends Transporte{

    public function Mantenimiento(){
        echo "</br>3.- Mantenimiento a una bicicleta</br>";
        echo "</br>Revisión de transmisión</br>";
        echo "</br>Revisión de frenos</br>";
        echo "</br>Revisión de ruedas</br>";
    }


}

Class Motocicleta extends Transporte{

    public function Mantenimiento(){
        echo "</br>4.- Mantenimiento a una motocicleta:</br>";
        echo "</br>Revisión de batería</br>";
        echo "</br>Comprobar las pastillas de freno y la cadena</br>";
        echo "</br>Limpieza y lubricación</br>";
    }


}


$obj = new Avion();
$obj2 = new Helicoptero();
$obj3 = new Bicicleta();
$obj4 = new Motocicleta();
$obj->Mantenimiento();
$obj2->Mantenimiento();
$obj3->Mantenimiento();
$obj4->Mantenimiento();
?>