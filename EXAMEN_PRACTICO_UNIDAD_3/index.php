<?php
class Perro{
    public $color ="Perrito café";
    public $dormir = "Perrito mimido";

    function construct__($color){
        $this->color = $color;
    }

    private function comer(){
        echo "El perro ta comiendo ";
    }

    protected function comida(){
        echo "croquetas";
    }

    public function EchoComer(){
        echo $this->comer();
    }

    public function EchoComida(){
        echo $this->comida(); 
    }

    function destruct__($dormir){
        $this->dormir = $dormir;
    }

}
class Plato extends Perro{
    public $material = "Platito de metal";
    public $contenido = "El plato ta vacío";

    function construct__($material){
        $this->material = $material;
    }

    public function ContenidoPlato(){
        return "El plato tiene ".$this->comida(); 
    }

    function destruct__($contenido){
        $this->contenido = $contenido;
    }
}

$Bulldog = New Perro;
$Platito = New Plato;

echo $Bulldog->color.'<br>';
echo $Platito->material.'<br>';
echo $Bulldog->EchoComer().$Bulldog->EchoComida().'<br>';
echo $Platito->ContenidoPlato().$Platito->EchoComida().'<br>';
echo $Bulldog->dormir.'<br>';
echo $Platito->contenido.'<br>';

?>