<?php

class microondas{

     public $marca = "";
     public $tamaño = "";
     public $botones = 0;
     public $potencia = 0;

     public function setPotencia($mhz_potencia){
       $this ->potencia = $mhz_potencia;
     }

     public function getPotencia (){
       return $this ->potencia."mhz";
      }
      public function setTamaño($cm_tamaño){
        $this ->tamaño = $cm_tamaño;
      }
    
    public function getTamaño (){
        return $this ->tamaño."cm";
       }
    
    }

$objWhirpool = new microondas;
$objToshiba = new microondas;
$objDaewoo = new microondas;

$objWhirpool->setPotencia(1200);
$objToshiba->setPotencia(800);
$objDaewoo->setPotencia(500);

echo "Potencia de la WHIRPOOL: ". $objWhirpool->getPotencia()."<br>";

echo "Potencia de la TOSHIBA: ". $objToshiba->getPotencia()."<br>";

echo "Potencia de la DAEWOO: ". $objDaewoo->getPotencia()."<br>";

$objWhirpool->setTamaño(48);
$objToshiba->setTamaño(40);
$objDaewoo->setTamaño(35);

echo "Altura de la WHIRPOOL: ". $objWhirpool->getTamaño()."<br>";

echo "Altura de la TOSHIBA: ". $objToshiba->getTamaño()."<br>";

echo "Altura de la DAEWOO: ". $objDaewoo->getTamaño()."<br>";

?>