<?php 
// Creación de la Clase "Conversiones", donde se encuentran las funciones del programa
class Conversiones{
// Objeto que define el resultado final, dependiendo de la función elegida
    public $ConversionRealizar = "";

// Las funciones del programa, las cuales sirven en este caso para hacer conversiones dependiendo de
// la variable x
    public function FC($x){
        return ($x - 32) * 5/9; 
    }

    public function CF($x){
        return $x * 1.8 + 32; 
    }

    public function FK($x){
        return ($x - 32) * 5/9 + 273.15; 
    }

    public function KF($x){
        return ($x - 273.15) * 1.8 + 32; 
    }
    public function CK($x){
        return $x + 273.15; 
    }
    public function KC($x){
        return $x - 273.15; 
    }

// La función del resultado del programa, el cual sale dependiendo de la conversión elegida y de la variable x
    public function ResultadoConversion($x){

        switch ($this->ConversionRealizar) {
            case 'fahrenheit-celsius':
                return $this->FC($x); 
                break;
            case 'celsius-fahrenheit':
                return $this->CF($x); 
                break;
            case 'fahrenheit-kelvin':
                return $this->FK($x); 
                break;
            case 'kelvin-fahrenheit':
                return $this->KF($x); 
                break;
            case 'celsius-kelvin':
                return $this->CK($x); 
                break;
            case 'kelvin-celsius':
                return $this->KC($x); 
                break;
            default:
                return 'Conversion sin definir';  
                break;
        }

    }

}




 
 
 ?>