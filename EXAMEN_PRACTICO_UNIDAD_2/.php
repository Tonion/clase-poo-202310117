<?php

class Producto {
 
 public $nombre;
 public $precio;
  
 function __construct($nombre, $precio = 0) {
 $this->nombre = $nombre;
 $this->precio = $precio;
 }
  
 function resumen() {
 return $this->nombre . " " . $this->precio;
 }
  
 function pvp() {
 return $this->precio * 1.21;
 }
  
 }
  
 class Cd extends Producto {
 public $longitud;
 }
  
 $nuevoCd=new Cd('Reggeton mix');
 $nuevoCd->longitud=90;
 $nuevoCd->precio=20;
 var_dump($nuevoCd);
  
 //Crear una clase libro con propiedad páginas que derive de producto
  
 class Libro extends Producto{
 public $paginas;
 //Sobreescribir el constructor añadiendo el parámetro páginas
 function __construct($nombre, $paginas, $precio = 0) {
 parent::__construct($nombre, $precio);
 $this->paginas=$paginas;
 }
 //Sobreescribimos el método de la clase madre
 function resumen(){
 //Invocamos el método de la clase madre
 return parent::resumen()." ".$this->paginas;
 }
 }
  
 class LibroAntiguo extends Libro{
 public $anyo;
 function __construct($nombre, $paginas,$anyo, $precio = 0) {
 parent::__construct($nombre, $paginas, $precio);
 $this->anyo=$anyo;
 }
 }
  
 $nuevoLibro=new Libro('El Quijote',400);
  
 echo $nuevoLibro->resumen();
 $antiguo=new LibroAntiguo('Quijote',500,1754);
 echo $antiguo->resumen();